import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../decorators/roles.decorator';
import { UserLoginDTO } from '../users/user-login-dto';
import { RolesGuard } from './../guards/roles.guard';
import { TicketService } from './auth.service';

@Controller('auth')
export class AuthController {
  public constructor(private readonly authService: TicketService) {}

  @Get()
  @Roles('admin', 'user')
  @UseGuards(AuthGuard(), RolesGuard)
  public root(): string {
    return 'root';
  }

  @Post('login')
  public async login(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    user: UserLoginDTO,
  ): Promise<string> {
    const token = await this.authService.login(user);
    if (!token) {
      throw new BadRequestException('Wrong credentials!');
    }

    return JSON.stringify(token);
  }
}
