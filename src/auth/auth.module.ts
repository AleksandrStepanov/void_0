import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UsersService } from '../users/users.service';
import { ConfigModule } from './../config/config.module';
import { ConfigService } from './../config/config.service';
import { AuthController } from './auth.controller';
import { TicketService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpireTime,
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [TicketService, JwtStrategy, UsersService],
  exports: [TicketService],
  controllers: [AuthController],
})
export class AuthModule {}
