import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Tickets } from './../entities/tickets';
import { TicketsService } from './../tickets/tickets.service';
import { UsersUpdateDTO } from './users-register-dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  public constructor(private readonly userService: UsersService, private readonly ticketService: TicketsService) {}

  @Get()
  public async getAllUsers(): Promise<string> {

    return JSON.stringify(await this.userService.findAllUsers());
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':email/tickets')
  public async getAllUserTickets(@Param('email') email: string): Promise<Tickets[]> {

    return this.ticketService.findAllUserTickets(email);
  }

  @Get(':email')
  public async getUserById(@Param('email') email: string): Promise<string> {

    return JSON.stringify(await this.userService.findUser(email));
  }

  @Post()
  public async createNewAcc(@Body() user: UsersUpdateDTO): Promise<string> {
    const { firstName, lastName, email, password } = user;

    return this.userService.addNewUser(firstName, lastName, email, password);
  }
}
