import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { getManager } from 'typeorm';
import { IJwtPayload } from '../auth/interfaces/jwt-payload';
import { Roles, Users } from './../entities';
import { GetUserDTO } from './get-user.dto';
import { UserLoginDTO } from './user-login-dto';

@Injectable()
export class UsersService {
  public async signIn(user: UserLoginDTO): Promise<GetUserDTO> {
    const userFound: GetUserDTO = await getManager().findOne(Users, {
      where: { email: user.email },
    });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }
    return null;
  }
  public async validateUser(payload: IJwtPayload): Promise<GetUserDTO> {
    const userFound: any = await getManager().findOne(Users, {
      where: { email: payload.email },
    });

    return userFound;
  }
  public async findAllUsers(): Promise<Users[]> {
    return getManager()
      .createQueryBuilder(Users, 'u')
      .select(['u.firstName', 'u.lastName', 'u.lastLogin'])
      .orderBy({ lastLogin: 'DESC' })
      .getMany();
  }

  public async findUser(userEmail: string): Promise<Users> {
    return getManager()
      .createQueryBuilder(Users, 'u')
      .select(['u.lastName', 'u.firstName', 'u.email'])
      .where('u.email = :email', { email: userEmail })
      .getOne();
  }

  public async addNewUser(
    newFirstName: string,
    newLastName: string,
    newEmail: string,
    newPassword: string,
    newRoleId?: number,
    isBanned?: boolean,
  ): Promise<string> {
    const isEmailExist = await this.findUser(newEmail);
    if (isEmailExist) {
      return `user with ${newEmail} already registered`;
    }
    const newUser = await getManager()
      .createQueryBuilder()
      .insert()
      .into(Users)
      .values({
        firstName: newFirstName,
        lastName: newLastName,
        email: newEmail,
        // tslint:disable-next-line:no-magic-numbers
        password: await bcrypt.hash(newPassword, 10),
        lastLogin: '1999-01-01',
        banned: isBanned || false,
      })
      .execute();

    getManager()
      .createQueryBuilder()
      .relation(Roles, 'users')
      .of(newRoleId || 2)
      .add(newUser.identifiers[0].userId);
    return 'new user successfully added';
  }
}
