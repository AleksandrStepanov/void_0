import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Tickets } from './tickets';

@Entity()
export class Locations {
  @PrimaryGeneratedColumn('uuid')
  // tslint:disable-next-line:variable-name
  public locationId: string;

  @Column({ type: 'nvarchar', length: 40, unique: true })
  public name: string;

  @OneToMany(() => Tickets, (tickets) => tickets.location)
  public tickets: Tickets[];
}
