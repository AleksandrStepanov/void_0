import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Locations } from './locations';
import { SubCategories } from './sub-categories';
import { Users } from './users';

@Entity()
export class Tickets {
  @PrimaryGeneratedColumn('uuid')
  public ticketId: string;

  @Column({ type: 'nvarchar', length: 200 })
  public name: string;

  @ManyToOne(() => Users, (users) => users.createdTickets)
  @JoinColumn({ name: 'creator' })
  public creator: Users;

  @ManyToOne(() => Users, (users) => users.receivedTickets)
  @JoinColumn({ name: 'receiver' })
  public receiver: Users;

  @Column({ type: 'tinyint' })
  public ask: boolean;

  @Column({ type: 'tinyint', default: 1 })
  public active: boolean;

  @Column({ type: 'text' })
  public description: string;

  @Column({ type: 'varchar', default: null })
  public imageUrl: string;

  @Column({ type: 'tinyint', default: 0 })
  public realized: boolean;

  @Column({ type: 'datetime' })
  public dateCreated: Date;

  @Column({ type: 'datetime' })
  public dateModified: Date;

  @Column({ type: 'datetime', default: null })
  public dateRealized: Date;

  @ManyToOne(() => Locations, (locations) => locations.tickets, { cascade: true, eager: true })
  @JoinColumn({ name: 'location' })
  public location: Locations;

  @ManyToOne(() => SubCategories, (subCategories) => subCategories.tickets, { cascade: true })
  @JoinColumn({ name: 'category' })
  public category: SubCategories;
}
