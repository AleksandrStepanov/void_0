import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Users } from './users';

@Entity()
export class Roles {
  @PrimaryGeneratedColumn('increment')
  public roleId: number;

  @Column({ type: 'varchar' })
  public role: string;

  @OneToMany(() => Users, (user) => user.role)
  public users: Users[];
}
