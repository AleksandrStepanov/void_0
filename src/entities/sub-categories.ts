import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Categories } from './categories';
import { Tickets } from './tickets';

@Entity()
export class SubCategories {
  @PrimaryGeneratedColumn('uuid')
  public subCategoryId: string;

  @Column({ type: 'nvarchar', length: 50 })
  public name: string;

  @OneToMany(() => Tickets, (tickets) => tickets.category)
  public tickets: Tickets[];

  @ManyToOne(() => Categories, (categories) => categories.subCategories)
  public mainCategory: Categories;
}
