import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { SubCategories } from './sub-categories';

@Entity()
export class Categories {
  @PrimaryGeneratedColumn('uuid')
  public categoryId: string;

  @Column({ type: 'nvarchar', nullable: false })
  public name: string;

  @OneToMany(() => SubCategories, (subCategories) => subCategories.mainCategory)
  public subCategories: SubCategories[];
}
