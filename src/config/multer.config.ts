import { HttpException, Injectable, MulterModuleOptions, MulterOptionsFactory } from '@nestjs/common';
import { Request } from 'express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { v4 } from 'uuid';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  public createMulterOptions(): MulterModuleOptions {
    return {
      dest: 'images/tickets',
      limits: {
        files: 1,
        // tslint:disable-next-line:no-magic-numbers
        fileSize: 2 * 1024 * 1024,
      },
      storage: diskStorage({
        destination: (req, file, cb) =>
          cb(null, join('.', 'assets', 'images', 'tickets')),
        filename: (req, file, cb) => {
          const fileName = `${v4()}${extname(file.originalname)}`;
          cb(null, fileName);
        },
      }),
      fileFilter: this.fileFilter,
    };
  }

  private fileFilter(req: Request, file: any, cb: any): any {
    const exts = ['.png', '.jpg', '.gif', '.jpeg'];
    if (exts.indexOf(extname(file.originalname)) !== -1) {
      return cb(null, true);
    }
    return cb(new HttpException('Extension not allowed', 400), false);
  }
}
