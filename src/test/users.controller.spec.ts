import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TicketsService } from '../tickets/tickets.service';
import { UsersUpdateDTO } from '../users/users-register-dto';
import { UsersController } from './../users/users.controller';
import { UsersService } from './../users/users.service';

describe('Users Controller should', () => {
  let usersController: UsersController;
  let ticketService: TicketsService;
  let userService: UsersService;
  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([])],
      providers: [
        {
          provide: TicketsService,
          useValue: {
            findAllUserTickets: (pepi) => pepi,
          },
        },
        {
          provide: UsersService,
          useValue: {
            findAllUsers: (pepi) => pepi,
            findUser: (pepi) => pepi,
            addNewUser: (pepi) => pepi,
          },
        },
      ],
      controllers: [UsersController],
    }).compile();

    userService = module.get<UsersService>(UsersService);
    usersController = module.get<UsersController>(UsersController);
    ticketService = module.get<TicketsService>(TicketsService);
    jest.doMock('express', () => {
      return '1';
    });
  });

  describe('getAllUsers should', () => {
    it('be called', () => {
      // Arrange
      jest.spyOn(usersController, 'getAllUsers');

      // Act
      usersController.getAllUsers();

      // Assert
      expect(usersController.getAllUsers).toBeCalled();
    });
  });

  describe('getAllUserTickets should', () => {
    it('Be called', () => {
      // Arrange

      jest.spyOn(usersController, 'getAllUserTickets');

      // Act
      usersController.getAllUserTickets('Pepi');

      // Assert
      expect(usersController.getAllUserTickets).toBeCalled();
    });

    it('Call findAllUserTickets method', () => {
      // Arrange

      jest.spyOn(ticketService, 'findAllUserTickets');

      // Act
      usersController.getAllUserTickets('Pepi');

      // Assert
      expect(ticketService.findAllUserTickets).toBeCalled();
    });
  });

  describe('getUserById should', () => {
    it('Be called', () => {
      // Arrange

      jest.spyOn(usersController, 'getUserById');

      // Act
      usersController.getUserById('Pepi');

      // Assert
      expect(usersController.getUserById).toBeCalled();
    });

    it('findUser must be called', () => {
      // Arrange
      jest.spyOn(userService, 'findUser');
      // Act
      usersController.getUserById('ala');
      usersController.getUserById('bala');
      usersController.getUserById('portokala');
      // Assert
      // tslint:disable-next-line:no-magic-numbers
      expect(userService.findUser).toBeCalledTimes(3);
    });
  });
  describe('createNewAcc should', () => {
    it('be called', () => {
      // Arrange
      jest.spyOn(usersController, 'createNewAcc');

      // Act
      usersController.createNewAcc(new UsersUpdateDTO());

      // Assert
      expect(usersController.createNewAcc).toBeCalled();
    });
    it('to call addNewUser', () => {
      // Arrange
      jest.spyOn(userService, 'addNewUser');

      // Act
      usersController.createNewAcc(new UsersUpdateDTO());

      // Assert
      // tslint:disable-next-line:no-magic-numbers
      expect(userService.addNewUser).toBeCalled();
    });
  });
});
