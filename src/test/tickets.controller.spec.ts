import { JwtService } from '@nestjs/jwt';
import { Test } from '@nestjs/testing';
import { TicketsController } from '../tickets/tickets.controller';
import { TicketsService } from '../tickets/tickets.service';
import { UsersService } from './../users/users.service';

describe('Tickets Controller', () => {
  let ticketsController: TicketsController;
  let ticketService: TicketsService;
  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [TicketsController],
      providers: [
        {
          provide: TicketsService,
          useValue: {
            getAllCategories: () => '',
            getAllLocations: () => '',
            getTicketById: (pepi) => pepi,
          },
        },
        { provide: UsersService, useValue: {} },
        { provide: JwtService, useValue: {} },
      ],
    }).compile();
    ticketsController = module.get<TicketsController>(TicketsController);
    ticketService = module.get<TicketsService>(TicketsService);
  });
  describe('getLocations should', () => {
    it('be called', async () => {
      // Arrange
      jest.spyOn(ticketsController, 'getLocations');

      // Act
      ticketsController.getLocations();

      // Assert
      expect(ticketsController.getLocations).toBeCalled();
    });
    it('call the ticketService', async () => {
      // Arrange
      jest.spyOn(ticketService, 'getAllLocations');

      // Act
      ticketsController.getLocations();

      // Assert
      expect(ticketService.getAllLocations).toBeCalled();
    });
  });
  describe('getCategories should', () => {
    it('be called', async () => {
      // Arrange
      jest.spyOn(ticketsController, 'getCategories');

      // Act
      ticketsController.getCategories();

      // Assert
      expect(ticketsController.getCategories).toBeCalled();
    });
    it('call ticketService', async () => {
      // Arrange
      jest.spyOn(ticketService, 'getAllCategories');

      // Act
      ticketsController.getCategories();

      // Assert
      expect(ticketService.getAllCategories).toBeCalled();
    });
  });
  describe('getTicketById should', () => {
    it('be called', async () => {
      // Arrange
      jest.spyOn(ticketsController, 'ticketById');

      // Act
      ticketsController.ticketById('123');

      // Assert
      expect(ticketsController.ticketById).toBeCalled();
    });
    it('call ticketService', async () => {
      // Arrange
      jest.spyOn(ticketService, 'getTicketById');

      // Act
      ticketsController.ticketById('123');

      // Assert
      expect(ticketService.getTicketById).toBeCalled();
    });
    it('call ticketService with right parameter', async () => {
      // Arrange
      jest.spyOn(ticketService, 'getTicketById');

      // Act
      ticketsController.ticketById('123');

      // Assert
      expect(ticketService.getTicketById).toBeCalledWith('123');
    });
  });
});
