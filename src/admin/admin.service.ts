import { getManager } from 'typeorm';
import { Locations, Roles } from '../entities';

export class AdminService {
  public getRoles(): Promise<any[]> {
    return getManager().find(Roles);
  }
  public getLocations(): Promise<any[]> {
    return getManager().find(Locations);
  }

  public addLocation(newLocation: string): void {
    getManager()
      .createQueryBuilder()
      .insert()
      .into('Locations')
      .values(newLocation)
      .execute();
  }
}
