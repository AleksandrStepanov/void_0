import {
  Body,
  Controller,
  Get,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { Roles } from '../decorators/roles.decorator';
import { RolesGuard } from './../guards/roles.guard';
import { AdminService } from './admin.service';

@Roles('admin')
@UseGuards(AuthGuard(), RolesGuard)
@Controller('admin')
export class AdminController {
  public constructor(private readonly adminService: AdminService) {}

  @Get()
  public root(): string {
    return 'welcome to admin part';
  }
  @Get('roles')
  public async getRoles(@Res() res: Response): Promise<void> {
    res.send(await this.adminService.getRoles());
  }
  @Get('locations')
  public async getLocations(@Res() res: Response): Promise<void> {
    res.send(await this.adminService.getLocations());
  }
  @Post('locations')
  public async addLocation(
    @Res() res: Response,
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    newLocation: string,
  ): Promise<void> {
    res.send(await this.adminService.addLocation(newLocation));
  }
}
