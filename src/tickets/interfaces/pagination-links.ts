export interface IPaginationQueries {
    first: {page: number; per_page: number};
    last: {page: number; per_page: number};
    prev?: {page: number; per_page: number};
    next?: {page: number; per_page: number};
}
