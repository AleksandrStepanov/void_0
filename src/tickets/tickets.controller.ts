import {
  Body,
  Controller,
  FileInterceptor,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,

} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from 'express';
import { join } from 'path';
import { Tickets, Locations, Categories } from '../entities';
import { NewTicketDTO } from './dtos/new-ticket.dto';
import { TicketUpdateDTO } from './dtos/ticket-update.dto';
import { IPaginationQueries } from './interfaces/pagination-links';
import { TicketsService } from './tickets.service';

@Controller('tickets')
export class TicketsController {
  public constructor(private readonly ticketService: TicketsService) {}

  @Get()
  public async tickets(
    @Res() res: Response,
    @Query() query: any,
  ): Promise<void> {
    // tslint:disable-next-line:no-magic-numbers
    const perPage = +query.per_page || 6;
    const tickets = await this.ticketService.findNewestTickets(query.page || 1, perPage, query.ask, query.cat);
    const ticketsCount = await this.ticketService.getTicketsCount(query.ask, query.cat);
    res.setHeader('Link', JSON.stringify(await this.setPaginationQueries(query, ticketsCount)));
    res.setHeader('access-control-expose-headers', 'Link');
    res.send(tickets);
  }

  @Get('locations')
  public async getLocations(): Promise<Locations[]> {
    return this.ticketService.getAllLocations();
  }

  @Get('categories')
  public async getCategories(): Promise<Categories[]> {
    return this.ticketService.getAllCategories();
  }

  @Get(':id')
  public async ticketById(@Param('id') ticketId: string): Promise<Tickets> {
    return this.ticketService.getTicketById(ticketId);
  }
  @UseGuards(AuthGuard('jwt'))
  @Post(':id')
  public async updateTicket(
    @Param('id') ticketId: string,
    @Req() req: Request,
    @Body() updateDto: TicketUpdateDTO): Promise<string> {
    const role = req.user.role.role;
    const isOwner = await this.ticketService.checkTicketOwner(ticketId, req.user.email);
    if (isOwner || role === 'admin' || role === 'moderator') {
      try {
        await this.ticketService.updateTicket(ticketId, updateDto);
      } catch (error) {
        return JSON.stringify(error);
      }
      const props = Object.keys(updateDto);
      if (props.length > 0) {
        return JSON.stringify({
          success: `Successfully updated following properties of the ticket: ${props.join(', ')}`,
        });
      } else {
        return JSON.stringify({
          noUpdate: 'Nothing updated!',
        });
      }
    }

  }

  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileInterceptor('image'))
  @Post()
  public async createTicket(
    @UploadedFile() image: any,
    @Body() ticketDto: NewTicketDTO,
    @Res() res: Response,
  ): Promise<void> {
    let imageUrl = null;
    if (image) {
          imageUrl = join('.', 'images', 'tickets', image.filename);
          const response = await this.ticketService.createTicket(
            ticketDto.name,
            ticketDto.ask,
            ticketDto.description,
            imageUrl,
            ticketDto.location,
            ticketDto.email,
            ticketDto.category,
            ).catch((error) => error);
          console.log(response);
          res.send(JSON.stringify(response));
    } else {
      const httpStatus: HttpStatus = 400;
      throw new HttpException(
        'It is required to include an image!',
        httpStatus,
      );
    }
  }

  private async setPaginationQueries(query: any, totalTicketCount: number): Promise<IPaginationQueries> {
    // tslint:disable-next-line:no-magic-numbers
    const perPage = +query.per_page || 6;
    const page = +query.page || 1;

    const lastPage = Math.floor(totalTicketCount / perPage) + 1;

    const paginationQueries: IPaginationQueries = {
      first: { page: 1, per_page: perPage },
      last: { page: lastPage, per_page: perPage },
      next: { page, per_page: perPage },
      prev: { page, per_page: perPage },
    };

    if (page > 1) {
      paginationQueries.prev.page = page - 1;
    }
    if (page < lastPage) {
      paginationQueries.next.page = page + 1;
    }

    return paginationQueries;
  }
}
