import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';
import {
  Categories,
  Locations,
  SubCategories,
  Tickets,
  Users,
} from '../entities';
import { TicketUpdateDTO } from './dtos/ticket-update.dto';

@Injectable()
export class TicketsService {
  public async createTicket(
    name: string,
    ask: boolean,
    description: string,
    imageUrl: string,
    location: string,
    userEmail: string,
    subCategory: string,
  ): Promise<{ id: string }> {
    const ticketToCreate = new Tickets();
    ticketToCreate.ask = ask;
    ticketToCreate.name = name;
    ticketToCreate.dateCreated = new Date();
    ticketToCreate.dateModified = new Date();
    ticketToCreate.description = description;
    ticketToCreate.imageUrl = imageUrl || null;

    ticketToCreate.location = await getManager()
      .createQueryBuilder(Locations, 'l')
      .where('l.name = :name', { name: location })
      .getOne();

    ticketToCreate.creator = await getManager()
      .createQueryBuilder(Users, 'u')
      .where('u.email = :email', { email: userEmail })
      .getOne();

    ticketToCreate.category = await getManager()
      .createQueryBuilder(SubCategories, 'cat')
      .where('cat.name = :name', { name: subCategory })
      .getOne();

    await getManager()
      .getRepository(Tickets)
      .create(ticketToCreate);
    await getManager()
      .getRepository(Tickets)
      .save(ticketToCreate);

    return { id: ticketToCreate.ticketId };
  }

  public async findAllUserTickets(userEmail: string): Promise<Tickets[]> {
    return (
      getManager()
        .createQueryBuilder(Tickets, 'tickets')
        // tslint:disable-next-line:no-duplicate-string
        .select([
          'tickets.name',
          'tickets.creator',
          'tickets.ticketId',
          'tickets.description',
          'tickets.ask',
          'tickets.imageUrl',
          'tickets.dateCreated',
          'tickets.dateModified',
        ])
        .leftJoin('tickets.creator', 'c')
        .leftJoin('tickets.location', 'loc')
        .leftJoin('tickets.category', 'cats')
        .where('c.email = :email', { email: userEmail })
        .addSelect(['c.email', 'c.firstName', 'c.lastName'])
        .addSelect(['loc.name'])
        .addSelect(['cats.name'])
        .getMany()
    );
  }

  public async findNewestTickets(
    page: number,
    perPage: number,
    isAsk?: boolean,
    category?: string,
  ): Promise<Tickets[]> {
    return getManager()
      .createQueryBuilder(Tickets, 'tickets')
      .select([
        'tickets.name',
        'tickets.creator',
        'tickets.ticketId',
        'tickets.description',
        'tickets.ask',
        'tickets.imageUrl',
        'tickets.dateCreated',
        'tickets.dateModified',
      ])
      .leftJoin('tickets.creator', 'creator')
      .leftJoin('tickets.location', 'loc')
      .leftJoin('tickets.category', 'cats')
      .where('cats.name like :cat', { cat: category || '%' })
      .andWhere('tickets.ask = :ask', { ask: isAsk })
      .orderBy({ 'tickets.dateCreated': 'DESC' })
      .offset(page * perPage - perPage)
      .limit(perPage)
      .addSelect(['creator.email', 'creator.firstName', 'creator.lastName'])
      .addSelect(['loc.name'])
      .addSelect(['cats.name'])
      .getMany();
  }

  public async getTicketById(ticketId: string): Promise<Tickets> {
    return getManager()
      .createQueryBuilder(Tickets, 'tickets')
      .where('tickets.ticketId = :id', { id: ticketId })
      .select([
        'tickets.name',
        'tickets.creator',
        'tickets.ticketId',
        'tickets.description',
        'tickets.ask',
        'tickets.imageUrl',
        'tickets.dateCreated',
        'tickets.dateModified',
      ])
      .leftJoin('tickets.creator', 'creator')
      .leftJoin('tickets.location', 'loc')
      .leftJoin('tickets.category', 'cats')
      .addSelect(['creator.email', 'creator.firstName', 'creator.lastName'])
      .addSelect(['loc.name', 'loc.locationId'])
      .addSelect(['cats.name', 'cats.subCategoryId'])
      .getOne();
  }

  public async updateTicket(ticketId: string, options: TicketUpdateDTO): Promise<void> {
    try {
      await getManager()
      .createQueryBuilder(Tickets, 'tickets')
      .update(Tickets)
      .set({ ...options, dateModified: new Date() })
      .where('ticketId = :id', { id: ticketId })
      .execute();
    } catch (error) {
      throw new Error('Failed to update ticket');
    }
  }

  public async checkTicketOwner(
    ticketId: string,
    ownerEmail: string,
  ): Promise<boolean> {
    const ticket: Tickets = await this.getTicketById(ticketId);
    if (ticket && ticket.creator.email === ownerEmail) {
      return true;
    } else {
      return false;
    }
  }
  // tslint:disable-next-line:no-identical-functions
  public async getTicketsCount(
    isAsk?: boolean,
    category?: string,
  ): Promise<number> {
    return getManager()
      .createQueryBuilder(Tickets, 'tickets')
      .select([
        'tickets.name',
        'tickets.creator',
        'tickets.ticketId',
        'tickets.description',
        'tickets.ask',
        'tickets.imageUrl',
        'tickets.dateCreated',
        'tickets.dateModified',
      ])
      .leftJoin('tickets.creator', 'creator')
      .leftJoin('tickets.location', 'loc')
      .leftJoin('tickets.category', 'cats')
      .where('cats.name like :cat', { cat: category || '%' })
      .andWhere('tickets.ask = :ask', { ask: isAsk })
      .orderBy({ 'tickets.dateCreated': 'DESC' })
      .addSelect(['creator.email', 'creator.firstName', 'creator.lastName'])
      .addSelect(['loc.name'])
      .addSelect(['cats.name'])
      .getCount();
  }

  public async realizeTicket(
    ticketId: string,
    receiverEmail: string,
  ): Promise<void> {
    await getManager()
      .createQueryBuilder(Tickets, 'tickets')
      .update(Tickets)
      .set({
        active: false,
        realized: true,
        dateRealized: new Date(),
        receivers: await getManager()
          .createQueryBuilder(Users, 'user')
          .where('user.email = :email', { email: receiverEmail })
          .getOne(),
      })
      .where('ticketId = :id', { id: ticketId })
      .execute();
  }

  public async getAllLocations(): Promise<Locations[]> {
    return getManager()
      .createQueryBuilder(Locations, 'locs')
      .getMany();
  }

  public async getAllCategories(): Promise<Categories[]> {
    return getManager()
      .createQueryBuilder(Categories, 'cats')
      .leftJoinAndSelect('cats.subCategories', 'subCats')
      .getMany();

  }
}
