import { Allow, IsEmail, IsIn, IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class NewTicketDTO {
    @IsString()
    // tslint:disable-next-line:no-magic-numbers
    @MaxLength(50)
    public name: string;

    @IsIn(['1', '0'])
    public ask: boolean;

    @IsString()
    // tslint:disable-next-line:no-magic-numbers
    @MaxLength(200)
    public description: string;

    @IsIn([
    'Blagoevgrad',
    'Burgas',
    'Dobrich',
    'Gabrovo',
    'Haskovo',
    'Kardjali',
    'Kiustendil',
    'Lovech',
    'Montana',
    'Pazardjik',
    'Pernik',
    'Pleven',
    'Plovdiv',
    'Razgrad',
    'Ruse',
    'Shumen',
    'Silistra',
    'Sliven',
    'Smolyan',
    'Sofia',
    'Stara Zagora',
    'Targovishte',
    'Varna',
    'Veliko Tarnovo',
    'Vidin',
    'Vraca',
    'Yambol',
              ])
    public location: string;
    @IsEmail()
    public email: string;

    // tslint:disable-next-line:no-magic-numbers
    @MaxLength(100)
    public category: string;
}
