import { IsOptional, IsString } from 'class-validator';

export class TicketUpdateDTO {
  @IsString({ message: 'Location must be a string' })
  @IsOptional()
  public location: string;

  @IsOptional()
  public image: string;

  @IsOptional()
  public description: string;

  @IsOptional()
  public active: boolean;

  @IsOptional()
  public category: string;

  @IsOptional()
  public name: string;
}
