import { Module, MulterModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterConfigService } from '../config/multer.config';
import { Locations, SubCategories, Tickets, Users } from '../entities';
import { TicketsController } from './tickets.controller';
import { TicketsService } from './tickets.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Tickets, SubCategories, Locations, Users]),
    MulterModule.registerAsync({
      useClass: MulterConfigService,
    }),
  ],
  providers: [TicketsService],
  controllers: [TicketsController],
})
export class TicketsModule {}
