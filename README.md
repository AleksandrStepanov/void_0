<h4>Gift It Forward</h4>
<b>Web application for giving away items you don`t really need</b>  
</br>

```

  
#Running the app
  
$ npm install  
$ npm run start


```

```
#Running the tests

$ npm test

```


<b>Link to the frontend repository:</b>

https://gitlab.com/wideyedwonderer/void-0-client

*To connect locally you must be running the server on port 3000

*For best user expirience download and put the images in assets/images/tickets
https://drive.google.com/open?id=1MkkWf89AXIEyOixlAt9f3p-ymezyrBj4

*.env https://docs.google.com/document/d/1JoPgDLtqqdrbLXE_oBLX-Cnpk22-lIAPsV8vfgoxaqA/edit?usp=sharing

<b>Build with</b>

Server: <a href="https://nestjs.com/">NestJS</a>  
Database: <a href="https://www.mysql.com/">MySQL</a>  
Database hosting: <a href="https://www.digitalocean.com/">Digital Ocean</a>  

```
Licence: MIT
```